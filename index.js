const fs = require("fs")
const express = require("express")
const app = express()
const bodyParser = require("body-parser")
const ejs = require("ejs")
const jsonParser = bodyParser.json();

const {
    user_games, user_biodata, user_history
} = require("./models")

const requestTime = function (req, res, next) {
    const time = new Date()
    console.log("===     Information     ===")
    console.log(
        `You are accessing the ${req.method} - ${req.url} \nat ${time.toLocaleString()} `
    )
    next()
}

app.post("/login", jsonParser, (req, res) => {
    let reqUserEmail = req.body.email;
    let reqUserPass = req.body.password;

    let dataAdmin = JSON.parse(fs.readFileSync("./utils/admin.json", "utf-8"));

    if (reqUserEmail == dataAdmin.email && reqUserPass == dataAdmin.password) {
        res.send(`Welcome Admin`);
    } else {
        res.status(401).send(`Wrong email or password for Admin`);
    }
});

app.post("/users", jsonParser, async (req, res) => {
    try {
        let dataUser = await userGames.create({
            username: req.body.username,
            email: req.body.email,
            password: req.body.password,
        });
        let user_biodatas = await user_biodata.create({
            user_id: dataUser.id,
            name: req.body.name,
            address: req.body.address,
            phone_number: req.body.phoneNumber,
        })
        res.status(201).send(`User succesfully created`)
    } catch (error) {
        console.log(error)
        res.status(422).send(`Cant register user`)
    }
});

app.post("/users/:id/game-history", jsonParser, async (req, res) => {
    try {
        let data = await userHistory.create({
            user_id: dataUser.id,
            login_time: req.body.loginTime,
            play_time: req.body.logoutTime
        })
        res.status(201).send(data);
    } catch (error) {
        console.log(error);
        res.status(422).send(`cant create user game history`)
    }
})

app.get("/users", async (req, res) => {
    let data = await user_game.findAll();
    res.send(data);
});

app.get("/users/:id", async (req, res) => {
    try {
        let data = await user_games.findOne({
            where: { id: req.params.id },
        });
        if (data != null) {
            res.send(data)
        } else {
            res.status(404).send(`User not found`)
        }
    } catch (error) {
        console.log(error)
        res.send(`error`)
    }
});

app.get("/users/:id/biodata", async (req, res) => {
    try {
        let data = await user_biodata.findOne({
            where: { user_id: req.params.id },
        });
        if (data != null) {
            res.send(data)
        } else {
            res.status(404).send(`User not found`)
        }
    } catch (error) {
        console.log(error)
        res.send(`error`)
    }
});

app.get("/users/:id/game-history", async (req, res) => {
    try {
        let data = await user_history.findAll({
            where: { user_id: req.params.id },
        });
        res.send(data);
    } catch (error) {
        console.log(error)
        res.send(`error`)
    }
});

app.put("/users/:id/biodata", jsonParser, async (req, res) => {
    try {
        let data = await user_biodata.findOne({
            where: { user_id: req.params.id },
        })
        (data.name = req.body.name)
        (data.address = req.body.address)
        (data.phone_number = req.body.phoneNumber)
            await data.save();
        res.status(202).send(data)
    } catch (error) {
        console.log(error)
        res.send(`error`)
    }
});

app.put("/users/:id/game-history", jsonParser, async (req, res) => {
    try {
        let data = await user_history.findOne({
            where: { user_id: req.params.id },
        })
        (data.login_time = req.body.loginTime)
        (data.logout_time = req.body.logoutTime)
            await data.save();
        res.status(202).send(data)
    } catch (error) {
        console.log(error)
        res.send(`cant register user`)
    }
});

app.delete("/users/:id", async (req, res) => {
    try {
        let data = await User.findByPk(req.params.id);
        data.destroy();
        res.status(202).send(data);
    } catch (error) {
        console.log(error);
        res.status(422).send("UNABLE TO DELETE DATA");
    }
});

app.delete("/users/:id/biodata", async (req, res) => {
    try {
        let data = await user_biodata.findOne({
            where: { user_id: req.params.id },
        });
        data.destroy();
        res.status(202).send(data);
    } catch (error) {
        console.log(error)
        res.status(422).send("UNABLE TO DELETE DATA")
    }
});

app.delete("/users/:id/game-history", async (req, res) => {
    try {
        let data = await user_history.findOne({
            where: { id: req.params.id },
        });
        data.destroy()
        res.status(202).send(data)
    } catch (error) {
        console.log(error)
        res.status(422).send("UNABLE TO DELETE DATA")
    }
});

app.delete("/users/:id/game-histories", async (req, res) => {
    try {
        let data = await user_history.destroy({
            where: {
                user_id: req.params.id,
            },
        })
        res.status(202).send(`data deleted`)
    } catch (error) {
        console.log(error)
        res.status(422).send("UNABLE TO DELETE DATA")
    }
});

app.listen(PORT, () => {
    console.log(`The server is listening on port http://localhost:${PORT}`)
})
